/**
 * @author      : oli (oli@pop-os)
 * @file        : main
 * @created     : Monday May 22, 2023 20:00:24 BST
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <math.h>

typedef struct csideMatrix {
    int rank;
    int* dims;
    double* data;
} csideMatrix;

int csideRows(csideMatrix* matrix)
{
    assert(matrix->rank >= 1);
    return matrix->dims[0];
}

int csideCols(csideMatrix* matrix)
{
    assert(matrix->rank >= 2);
    return matrix->dims[1];
}

int csideLen(csideMatrix *matrix)
{
    int prod = 1;
    for (int i = 0; i < matrix->rank; i++) {
        prod *= matrix->dims[i];
    }
    return prod;
}

csideMatrix* csideNewMatrix(int rows, int cols)
{
    csideMatrix* m = malloc(sizeof(csideMatrix));
    m->rank = 2;
    m->dims = malloc(m->rank * sizeof(int));
    m->dims[0] = rows;
    m->dims[1] = cols;
    m->data = malloc(rows * cols * sizeof(double));
    return m;
}

typedef csideMatrix csideVector;

csideVector* csideNewVector(int dim)
{
    csideVector* v = malloc(sizeof(csideVector));
    v->rank = 1;
    v->dims = malloc(sizeof(int));
    v->dims[0] = dim;
    v->data = malloc(dim * sizeof(double));
    return v;
}

void csideFreeMatrix(csideMatrix* m)
{
    free(m->dims);
    free(m->data);
    free(m);
    m->dims = NULL;
    m->data = NULL;
    m = NULL;
}

double csideMatrixAccess(csideMatrix* m, int row, int col)
{
    return m->data[row * csideCols(m) + col];
}

void csideMatrixSet(csideMatrix* m, int row, int col, double val)
{
    m->data[row * csideCols(m) + col] = val;
}

csideMatrix* csideMatrixCopy(csideMatrix* m)
{
    csideMatrix *c;
    if (m->rank == 1) {
        c = csideNewVector(csideLen(m));
    } else if (m->rank == 2) {
        c = csideNewMatrix(csideRows(m), csideCols(m));
    }
    for (int i = 0; i < csideLen(m); i++) {
        c->data[i] = m->data[i];
    }
    return c;
}

void csideMatrixPrint(csideMatrix* m)
{
    for (int r = 0; r < csideRows(m); r++) {
        printf("[ ");
        for (int c = 0; c < csideCols(m); c++) {
            printf("%f", m->data[r * csideCols(m) + c]);
            if (c != csideCols(m) - 1)
                printf(", ");
        }
        printf(" ]\n");
    }
}
// for vector there it is rank 1
void csidePrint(csideMatrix *m)
{
    for (int idx = 0; idx < csideLen(m); idx++) {
        if (idx == 0) {
            printf("[ ");
        }
        for (int r = 1; r < m->rank; r++) {
            if (idx % m->dims[r] == 0 && idx != 0) {
                printf("] \n[ ");
            }
        }
        printf("%f, ", m->data[idx]);
    }
    printf("]\n\n");
}

csideMatrix * csideMatrixIdentity(size_t dim)
{
    csideMatrix* m = csideNewMatrix(dim, dim);
    for (int i = 0; i < dim * dim; i++) {
        m->data[i] = 0.0;
    }
    for (int i = 0; i < dim; i++) {
        m->data[i * dim + i] = 1.0;
    }
    return m;
}

void csideSetValue(csideMatrix *m, double value)
{
    for (int idx = 0; idx < csideLen(m); idx++) {
        m->data[idx] = value;
    }
}

void csideSetZeros(csideMatrix *m)
{
    csideSetValue(m, 0.0);
}

void csideSetOnes(csideMatrix *m)
{
    csideSetValue(m, 1.0);
}

void csideSetIdentity(csideMatrix *m)
{
    if (m->rank == 1) {
        csideSetOnes(m);
    } else if (m->rank == 2) {
        csideSetZeros(m);
        for (int i = 0; i < m->dims[0]; i++) {
            m->data[i * m->dims[0] + i] = 1.0;
        }
    }
}

void csideSetRandom(csideMatrix *m)
{
    for (int i = 0; i < csideLen(m); i++) {
        m->data[i] = (double)rand() / (double)RAND_MAX;
    }
}

csideMatrix * csideMatrixRandom(size_t rows, size_t cols)
{
    csideMatrix* m = csideNewMatrix(rows, cols);
    for (int i = 0; i < rows * cols; i++) {
        m->data[i] = (double)rand() / (double)RAND_MAX;
    }
    return m;
}

void csideMatrixAddInPlace(csideMatrix* a, csideMatrix* b)
{
    assert(csideRows(a) == csideRows(b) && csideCols(a) == csideCols(b));

    for (int i = 0; i < csideLen(a); i++)
        a->data[i] += b->data[i];
}

csideMatrix* csideMatrixAdd(csideMatrix* a, csideMatrix* b)
{
    csideMatrix* m = csideMatrixCopy(a);
    csideMatrixAddInPlace(m, b);
    return m;
}

csideMatrix* csideMatrixMatMul(csideMatrix* a, csideMatrix* b)
{
    assert(csideRows(a) == csideRows(b) && csideCols(a) == csideCols(b));

    csideMatrix * m = csideNewMatrix(csideRows(a), csideCols(a));
    for (int r = 0; r < csideRows(a); r++) {
        for (int c = 0; c < csideCols(a); c++) {
            m->data[r * csideCols(m) + c] = 0.0;
            for (int k = 0; k < csideCols(a); k++) {
                double aval = a->data[r * csideCols(a) + k];
                double bval = b->data[k * csideCols(b) + c];
                m->data[r * csideCols(m) + c] += aval * bval;
            }
        }
    }
    return m;
}

void csideScalarMulInPlace(csideMatrix* a, double b)
{
    for (int i = 0; i < csideLen(a); i++)
        a->data[i] *= b;
}

csideMatrix* csideScalarMul(csideMatrix* a, double b)
{
    csideMatrix* m = csideMatrixCopy(a);
    csideScalarMulInPlace(m, b);
    return m;
}

csideVector* csideMatVecMul(csideMatrix *m, csideVector *v)
{
    csideVector *vcopy = csideMatrixCopy(v);
    // check the ranks of m and v are correct
    // check dimensions of m and v are correct 
    for (int r = 0; r < csideRows(m); r++) {
        vcopy->data[r] = 0.0;
        for (int c = 0; c < csideCols(m); c++) {
            vcopy->data[r] += m->data[r * csideCols(m) + c] * v->data[c];
        }
    }
    return vcopy;
}

// Norms 
double csideL1Norm(csideMatrix *m)
{
    double norm = 0.0;
    for (int idx = 0; idx < csideLen(m); idx++) {
        norm += m->data[idx];
    }
    return norm;
}

double csideL2Norm(csideMatrix *m)
{
    double norm = 0.0;
    for (int idx = 0; idx < csideLen(m); idx++) {
        const double value = m->data[idx];
        norm += value * value;
    }
    return norm;
}

void csideNormalise(csideMatrix *m)
{
    // calc norm, then divide all elements by it 
    double norm = csideL2Norm(m);
    csideScalarMulInPlace(m, 1.0 / sqrt(norm));
}

double csideDotProd(csideVector *a, csideVector *b) 
{
    // check both rank 1 
    assert(a->rank == 1 && b->rank == 1);
    // check same length
    assert(csideLen(a) == csideLen(b));

    double sum = 0.0;
    for (int idx = 0; idx < csideLen(a); idx++) {
        sum += a->data[idx] * b->data[idx];
    }
    return sum;
}

csideMatrix* csideTranspose(csideMatrix *m)
{
    csideMatrix *mT = csideNewMatrix(csideCols(m), csideRows(m));
    for (int r = 0; r < csideRows(m); r++) {
        for (int c = 0; c < csideCols(m); c++) {
            mT->data[c * csideCols(mT) + r] = m->data[r * csideCols(m) + c];
        }
    }
    return mT;
}

int tOne(size_t dim)
{
    csideMatrix* m = csideNewMatrix(dim, dim);

    csideMatrix* id = csideMatrixIdentity(dim);

    csideMatrixPrint(m);
    printf("\n");
    csideMatrixPrint(id);
    printf("\n");

    csideMatrix* sum = csideMatrixAdd(id, id);
    csideMatrixPrint(sum);
    printf("\n");

    csideMatrix* rand = csideMatrixRandom(dim, dim);
    csideMatrixPrint(rand);
    printf("\n");

    csideMatrix* copy = csideMatrixCopy(rand);
    csideMatrixPrint(copy);
    printf("\n");

    csideFreeMatrix(m);
    csideFreeMatrix(id);
    csideFreeMatrix(sum);
    csideFreeMatrix(rand);
    csideFreeMatrix(copy);

    return 0;
}

int tTwo(size_t dim)
{
    csideMatrix* rand = csideMatrixRandom(dim, dim);
    csideMatrixPrint(rand);
    printf("\n");

    csideMatrix* Ident = csideMatrixIdentity(dim);
    csideMatrixAddInPlace(rand, Ident);
    csideMatrixPrint(rand);
    printf("\n");

    printf("Rand * Ident\n");
    csideMatrixPrint(rand);
    printf("\n");
    csideMatrixPrint(Ident);
    printf("\n");
    csideMatrix* prod = csideMatrixMatMul(rand, Ident);
    csideMatrixPrint(prod);
    printf("\n");

    csideScalarMulInPlace(prod, 2.0);
    csideMatrixPrint(prod);
    printf("\n");

    csideMatrix* prodCopy = csideScalarMul(prod, 0.5);
    csideMatrixPrint(prodCopy);

    csideFreeMatrix(rand);
    csideFreeMatrix(Ident);
    csideFreeMatrix(prod);
    csideFreeMatrix(prodCopy);
    return 0;
}

int main()
{
    const int dim = 4;
    // tOne(dim);
    // tTwo(dim);

    csideMatrix* a = csideMatrixRandom(dim, dim);
    csideMatrix* b = csideMatrixRandom(dim, dim);

    csideMatrix* c = csideMatrixMatMul(a, b);

    csideVector* v = csideNewVector(2);
    csidePrint(v);

    csidePrint(a);

    csideMatrixPrint(a);

    // make a random matrix 
    // make the all ones unit vector

    {
        printf("Here\n");
        csideMatrix *m = csideNewMatrix(dim, dim);
        csideSetIdentity(m);

        csideVector *v = csideNewVector(dim);
        csideSetIdentity(v);

        csidePrint(m);
        csidePrint(v);

        printf("l1 norm = %f \n", csideL1Norm(v));
        printf("l2 norm = %f \n", csideL2Norm(v));

        csideNormalise(v);
        csidePrint(v);

        printf("dot product of v,v %f \n", csideDotProd(v, v));

        csideSetRandom(v);
        csidePrint(v);

        csideNormalise(v);
        csidePrint(v);
        printf("dot product of v,v %f \n", csideDotProd(v, v));

        csideScalarMulInPlace(m, 2.0);

        printf("Mat, \n");
        csidePrint(m);
        printf("v, \n");
        csidePrint(v);

        printf("m * v\n");
        v = csideMatVecMul(m, v);
        csidePrint(v);

        csideMatrix *rmat = csideNewMatrix(dim, dim);
        csideSetRandom(rmat);
        csidePrint(rmat);

        csidePrint(csideTranspose(rmat));
    }


    return c->data[0];

    // return 0;
}

